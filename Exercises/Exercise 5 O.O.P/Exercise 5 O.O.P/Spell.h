#pragma once
#include <iostream>
#include<time.h>
#include<string>
#include <vector>
using namespace std;

class Wizard;


class Spell
{
public:

	Spell(string name, int power);

	int getPower();
	string getName();

	//string getSpellName(string characterName);
	int getSpellMinDamage(int spellMinDmg);
	int getSpellMaxDamage(int spellMaxDmg);
	int getSpellMpCost(int amount);

	//
	//void activate(Wizard* target);
	void printStats();
	

private:

	string mName;
	int mPower;
	//string mSpellName;
	int mSpellMinDmg;
	int mSpellMaxDmg;
	int mSpellMpCost;




};

