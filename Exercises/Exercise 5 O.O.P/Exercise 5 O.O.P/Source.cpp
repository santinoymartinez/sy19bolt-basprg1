#include <iostream>
#include<time.h>
#include<string>
#include <vector>
#include "Wizard.h"
#include "Spell.h"
using namespace std;


int main()
{
	srand(time(0));

	Wizard* wizard1 = new Wizard();
	wizard1->getName("Wizard 1");
	wizard1->setHp(250);
	wizard1->getMp(0);
	wizard1->getMinDamage(10);
	wizard1->getMaxDamage(15);

	Wizard* wizard2 = new Wizard();
	wizard2->getName("Wizard 2");
	wizard2->setHp(250);
	wizard2->getMp(0);
	wizard2->getMinDamage(10);
	wizard2->getMaxDamage(15);

	Spell* spell1 = new Spell("Fire", rand() % 20 + 40);
	//spell1->getSpellName("Wizard 1 Magic Attack");
	spell1->getSpellMaxDamage(60);
	spell1->getSpellMinDamage(40);
	spell1->getSpellMpCost(50);


	while (true)
	{
		wizard1->attack(wizard2);
		system("pause");
		wizard2->attack(wizard1);
		system("pause");
	
	
		
	}
}