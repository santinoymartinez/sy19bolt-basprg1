
#include "Wizard.h"
#include <iostream>
#include<time.h>
#include<string>
#include <vector>

#include "Spell.h"
using namespace std;

//
//
//string Spell::getSpellName(string spellName)
//{
//	mSpellName = spellName;
//	return mSpellName;
//}
//
int Spell::getSpellMinDamage(int minDmg)
{
	mSpellMinDmg = minDmg;
	return mSpellMinDmg;
}

int Spell::getSpellMaxDamage(int maxDmg)
{
	mSpellMaxDmg = maxDmg;
	return mSpellMaxDmg;
}

int Spell::getSpellMpCost(int amount)
{
	mSpellMpCost = amount;
	return mSpellMpCost;
}

//void Spell::activate(Wizard* target)
//{
//	//target->mHp -= rand () % 20 + 40;
//	cout << "Hello" << endl;
//}

Spell::Spell(string name, int power)
{
	mName = name;
	mPower = power;
}

int Spell::getPower()
{
	return mPower;
}

string Spell::getName()
{
	return mName;
}

void Spell::printStats()
{
	cout << mName << "" << mPower << endl;
}
