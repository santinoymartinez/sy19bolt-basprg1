#pragma once
#include <iostream>
#include<time.h>
#include<string>
#include <vector>
using namespace std;

class Spell;

class Wizard
{
public:
	

	string getName(string characterName);
	void setHp(int health);
	void setMp(int magic);
	int getMinDamage(int minDmg);
	int getMaxDamage(int maxDmg);
	int getHp(int ghealth);
	int getMp(int gmagic);

	Spell* getSpell();
	
	void attack(Wizard* target);

private:
	string mName;
	int mHp;
	int mMp;
	int mMinDamage;
	int mMaxDamage;

	Spell* mSpell;
};

