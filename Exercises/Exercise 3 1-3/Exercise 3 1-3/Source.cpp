#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;
//Ex 3 - 1 (Array as Pointer)
//Create a function which accepts an integer array in the form of a pointer as a parameter to the function.This function will fill the array with random values.
//
//Use int* numbers instead of int numbers[]

//
//Ex 3-1
int* randomarray;
int numbers[10];

//Ex 3-2
int* dynamicArray;
int n[10];
int b;


//Ex 3-3
int size;
int* newArray = new int;
int firstArray[10];

int main()
{
	cout << "Exersise 3 - 1" << endl;
	srand(time(0));
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = rand() % 500;
		int* randomarray = numbers;
		cout << randomarray[i] << endl;

	}
	system("pause");

	//Ex 3 - 2 (Creating a Dynamic Array)
	//Create a function which creates an integer array internally and fill that array with random values.
	//This function will return the created array back to the caller.
	//
	//Tips:
	//An array can be defined as a pointer to the first element.
	//Arrays cannot be used as a return type.However, a pointer to the array is allowed.

	cout << "Exersise 3 - 2" << endl;
	int b;
	cout << "Please enter the size of the number array." << endl;
	cin >> b;
	int* n = new int[b];
	int dynamicArray[10];
	srand(time(0));
	for (int i = 0; i < b; i++)
	{
		dynamicArray[i] = rand() % 400;
		cout << dynamicArray[i] << endl;
		system("pause");

		n[i] = dynamicArray[i];
		cout << n[i] << endl;
	}

	system("pause");




	//Ex 3 - 3 (Deleting dynamic elements of a dynamic array)
	//Create a function similar to 3 - 2 but each element is a dynamically allocated integer.
	//After calling the function, the array and all its elements must be deleted properly.


	int size;
	int* newArray = new int;
	int firstArray[10];
	cout << "Exersise 3 - 3" << endl;
	cout << "Please enter the size of the number array." << endl;
	cin >> size;

	srand(time(0));
	for (int i = 0; i < size; i++)
	{
		int counter;
		cout << "Enter number" << endl;
		cin >> counter;
		firstArray[i] = counter;
		newArray[i] = firstArray[i];
		cout << "New Array: " << newArray[i] << endl;
		system("pause");

	}

	delete[] newArray;
	newArray = nullptr;
	cout << newArray[0];




}