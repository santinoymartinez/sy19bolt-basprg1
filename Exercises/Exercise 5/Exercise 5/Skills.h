#pragma once
#include <iostream>
#include<time.h>
#include<string>
#include <vector>
#include "Unit.h"

using namespace std;
class Skills 
{
public:
	string getName();
	int getGained();
	Skills(string name, int amountGained);
	virtual int getComputation() = 0;

private:
	string mName;
	int amountGained;
};

