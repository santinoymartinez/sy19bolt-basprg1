#include "Unit.h"

string Unit::getName()
{
    return mName;
}

int Unit::getHp()
{
    return mHp;
}

int Unit::getPower()
{
    return mPower;
}

int Unit::getVitality()
{
    return mVitality;
}

int Unit::getDexterity()
{
    return mDexterity;
}

int Unit::getAgility()
{
    return mAgility;
}
