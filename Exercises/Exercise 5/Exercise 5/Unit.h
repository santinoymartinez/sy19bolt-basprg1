#pragma once
#include <iostream>
#include<time.h>
#include<string>
#include <vector>
using namespace std;
class Unit
{

public:
	Unit(string name, int hp, int power);

	string getName();
	int getHp();
	int getPower();
	int getVitality();
	int getDexterity();
	int getAgility();

	int stats;
	int printingStats();
	
	

private:
	string mName;
	int mHp;
	int mPower;
	int mVitality;
	int mDexterity;
	int mAgility;

};

