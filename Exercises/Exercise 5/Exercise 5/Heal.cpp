#include "Heal.h"

Heal::Heal(string name, int amountGained)
	:Skills("Heal", 10)
{
	mName = name;
	mGained = amountGained;

}

int Heal::getGained()
{
	return mGained;
}

string Heal::getName()
{
	return mName;
}

int Heal::getComputation()
{
	cout << "You have used " << mName << endl;
	return mGained;
}
