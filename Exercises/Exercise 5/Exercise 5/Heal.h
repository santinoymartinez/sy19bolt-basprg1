
#include "Skills.h"
#include <iostream>
#include<time.h>
#include<string>
#include <vector>
using namespace std;
class Heal : public Skills 
{
public:
	Heal(string name, int amountGained);
	//:Skills(name, amountGained)
	int getGained();
	string getName();
	int getComputation() override;
private:
	int mGained;
	string mName;
};

