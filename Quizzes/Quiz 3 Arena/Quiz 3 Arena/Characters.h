#pragma once
#include <string>
#include <iostream>
#include <time.h>
using namespace std;


class Characters
{
public:
	Characters(string name,string title);
	Characters();
	string getName();
	string getTitle();
	int getHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();
	int getTitleCounter(int title);
	int showEnemyCounter();

	
	int alive();
	int enemyalive();
	void battle(Characters* target);
	void enemyBattle(Characters* target);
	int takeDamage(int damage);
	int enemyDamage(int damage);

	

	int firstHit();
	
	
	//Upon Winning a Battle
	int statsBoost();
	int enemyStatsBoost(int boost);
	int healPlayer(int maxHealth);      

	//Enemy Behaviours and Others
	string getEnemyTitle();
	int getEnemyHp(); 
	int getEnemyPower();
	int getEnemyVitality();
	int getEnemyAgility();
	int getEnemyDexterity();
	string getEnemyTitleCounter(int randomChoice);
	int getEnemyTitleNumber(int randomChoice);


private:
	//Basic Class 
	int mRound;
	int mCharacterSelect;
	//Hero
	string mname;
	string mtitle;
	int mHp = rand () % 10 +20;
	int mPower =  rand () % 5 + 5; 
	int mVitality = rand() % 5 + 5; 
	int mAgility = rand() % 5 + 5; 
	int mDexterity = rand () % 5 + 5; 
	int mTitleCounter = 1;

	//Enemy
	string enemyTitle = "Enemy ";
	int enemyHp = 20;
	int enemyPower = rand() % 5 + 1;
	int enemyVitality = rand() % 5 + 1;
	int enemyAgility = rand() % 5 + 1;
	int enemyDexterity = rand() % 5 + 1;
	int enemyTitleCounter = 4;
};

