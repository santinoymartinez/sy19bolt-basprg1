#include <iostream>
#include<time.h>
#include<string>
#include <vector>
#include "Characters.h"

using namespace std;
 
int main()
{
	srand(time(NULL));
	string yourName;
	string title;
	int selection;
	int titleCounter;
	int round = 0;
//Your Name
	cout << "What is your name" << endl;
	cin >> yourName;
	system("pause");
	system("cls");
	//Choosing a class
	cout << "Select your class " << endl;
	cout << "[1] Warrior" << endl;
	cout << "[2] Assassin" << endl;
	cout << "[3] Mage" << endl;
	cin >> selection;
	system("pause");
	system("cls");
	// How to get your class
	if (selection == 1)
	{
		title = "Warrior";
		titleCounter = 1;
		

	}else if (selection == 2)
	{
		title = "Assassin";
		titleCounter = 2;

	}else if (selection == 3)
	{
		title = "Mage";
		titleCounter = 3;

	}
	//Print Sheet Main
	int maxhealth;
	Characters* character = new Characters(yourName, title);
	character->getTitleCounter(titleCounter);
	maxhealth = character->getHp();
	
	int i = 0;
	while (character->alive())
	{
		round++;
		Characters* enemy = new Characters;
		enemy->enemyStatsBoost(i);
		system("cls");
		cout << "Round " << round << endl;
		cout << "Name: "<< character->getName() << endl;
		cout << "Class: " <<character->getTitle() << endl;
		cout << "Hp " << character->getHp() << "/" << maxhealth << endl;
		cout <<"Power "<< character->getPower() << endl;
		cout << "Vitality " << character->getVitality() << endl;
		cout << "Agility " << character->getAgility() << endl;
		cout << "Dexterity " << character->getDexterity() << endl;

		int randomChoice = rand() % 3 + 1;
		int selection = randomChoice;
		enemy->getEnemyTitleCounter(selection);
		enemy->getEnemyTitleNumber(selection);
		cout << "" << endl;
		cout << "VS" << endl;
		cout << "" << endl;
		cout << enemy->getEnemyTitle() << endl;
		cout << "Enemy health " << enemy->getEnemyHp() << endl;
		cout << "Enemy power " << enemy->getEnemyPower() << endl;
		cout << "Enemy vitality " << enemy->getEnemyVitality() << endl;
		cout << "Enemy agility " << enemy->getEnemyAgility() << endl;
		cout << "Enemy dexterity " << enemy->getEnemyDexterity() << endl;
	
		system("pause");
		system("cls");
	/*	cout << "Counter You and  Enemy " << character->showEnemyCounter() << " " << enemy->showEnemyCounter() << endl;
		cout << "Counter You and  Enemy " << character->showEnemyCounter() << " " << enemy->showEnemyCounter() << endl;*/
		// Battle
		while (character->getHp() > 0 && enemy->getEnemyHp() > 0)
		{
			//Who go first?
		/*	cout << "Counter You and  Enemy " << character->showEnemyCounter() << " " << enemy->showEnemyCounter() << endl;*/
			if (character->getAgility() >= enemy->getEnemyAgility())
			{
				//1st Turn Hero
				if (character->alive())
				{
					character->enemyBattle(enemy);
					system("pause");
				}
				if (enemy->enemyalive())
				{
					enemy->battle(character);
					system("pause");
				}
			/*	cout << character->getHp() << endl;
				cout << enemy->getEnemyHp() << endl; */
			}
			else
			{
				//1st Turn Enemy
				if (enemy->enemyalive())
				{
					enemy->battle(character);
					system("pause");
				}
				if (character->alive())
				{
					character->enemyBattle(enemy);
					system("pause");
				}
			/*	cout << character->getHp() << endl;
				cout << enemy->getEnemyHp() << endl;*/
			}
		}
			system("pause");
			system("cls");
			if (character->alive())
			{
				
				character->healPlayer(maxhealth);
				character->statsBoost();
				i += rand() % 3 + 1;
			}
			else
			{
				cout << "Round " << round << endl;
				cout << "Name: " << character->getName() << endl;
				cout << "Class: " << character->getTitle() << endl;
				cout << "Hp " << character->getHp() << "/" << maxhealth << endl;
				cout << "Power " << character->getPower() << endl;
				cout << "Vitality " << character->getVitality() << endl;
				cout << "Agility " << character->getAgility() << endl;
				cout << "Dexterity " << character->getDexterity() << endl;
				system("pause");
			}

	}

}