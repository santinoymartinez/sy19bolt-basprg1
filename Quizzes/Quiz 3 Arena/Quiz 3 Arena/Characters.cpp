#include "Characters.h"
#include <string>
#include <iostream>
#include <time.h>

Characters::Characters(string name, string title)
{
	mname = name;
	mtitle = title;

}

Characters::Characters()
{
}



string Characters::getName()
{
	return mname;
}

string Characters::getTitle()
{
	return mtitle;
}

int Characters::getHp()
{
	if (mHp < 0)
	{
		mHp = 0;
	}
	return mHp;
}

int Characters::getPower()
{
	
	return mPower;
}

int Characters::getVitality()
{
	return mVitality;
}

int Characters::getAgility()
{
	return mAgility;
}

int Characters::getDexterity()
{
	return mDexterity;
}

int Characters::getTitleCounter(int title)
{

	mTitleCounter = title;
	return mTitleCounter;
}

int Characters::showEnemyCounter()
{
	return enemyTitleCounter;
}



int Characters::alive()
{
	return mHp > 0;
}

int Characters::enemyalive()
{
	return enemyHp > 0;
}

void Characters::battle(Characters* target)
{
	//Hit chance
	int hitchance;
	hitchance = (enemyDexterity / mAgility) * 100;

	if (hitchance > 80)
	{
		hitchance = 80;
	}
	else if (hitchance < 20)
	{
		hitchance = 20;
	}

	int attempt = rand() % 100 + 1;
	//cout << "Attempt " << attempt << endl;
	//cout << "Hit chance " << hitchance << endl;
	if (attempt >= hitchance)
	{
		cout << "The attack missed" << endl;
		return;
	}
	else
	{
		//Bonus Damge
		int bonusDamage = 1;
	/*	cout << " Hero counter " << target->mTitleCounter << endl;
		cout << "Enemy counter " << showEnemyCounter() << endl;*/
		if (target->mTitleCounter == 1 && enemyTitleCounter == 3)
		{
			bonusDamage = 1.5;
		}
		else if (target->mTitleCounter == 2 && enemyTitleCounter == 1)

		{
			bonusDamage = 1.5;
		}
		else if (target->mTitleCounter == 3 && enemyTitleCounter == 2)

		{
			bonusDamage = 1.5;
		}
		//Attack Me
		int damg = ((enemyPower - mVitality) * bonusDamage);
		//cout << "bonus Damgae" << bonusDamage << endl;
		target->takeDamage(damg);
		cout << target->mname << " was attacked and lost " << damg << " HP!" << endl;

	}
}

void Characters::enemyBattle(Characters* target)
{
	//Hit chance
	int hitchance;
	int attempt = rand() % 100 + 1;
	hitchance = (mDexterity / enemyAgility) * 100;
	if (hitchance > 80)
	{
		hitchance = 80;
	}
	else if (hitchance <= 20)
	{
		hitchance = 20;
	}
	/*cout << "Attempt " << attempt << endl;
	cout << "Hit chance " << hitchance << endl;*/
	if (attempt >= hitchance)
	{ 
		cout << "The attack missed" << endl;
		return;
	}
	else
	{
		//Bonus Damge
		int bonusDamage = 1;
	/*	cout << " Hero counter " << mTitleCounter << endl;
		cout << "Enemy counter " << target->showEnemyCounter() << endl;*/
		if (target->enemyTitleCounter == 2 && mTitleCounter == 1)
		{
			bonusDamage = 1.5;
		}
		else if (target->enemyTitleCounter == 3 && mTitleCounter == 2)

		{
			bonusDamage = 1.5;
		}
		else if (target->enemyTitleCounter == 1 && mTitleCounter == 3)

		{
			bonusDamage = 1.5;
		}
		int dmg = ((mPower - enemyVitality) * bonusDamage);
	/*	cout << "bonus Damgae" << bonusDamage << endl;*/
		target->enemyDamage(dmg);
		//Bad Guy Hurt
		cout << target->enemyTitle << " was attacked and lost " << dmg << " HP!" << endl;
	}
}

int Characters::takeDamage(int damage)
{
	//Power Minimum 1
	if (damage < 1)
	{
		damage = 1;
	}
		//Take Damage
	mHp -= damage;
	return	mHp;
	

}

int Characters::enemyDamage(int damage)
{
	// Power Minimum 1
		if (damage < 1)
		{
			damage = 1;
		}
enemyHp -= damage;
return enemyHp;
}

int Characters::firstHit()
{
	return 0;
}

int Characters::statsBoost()
{

	if (mTitleCounter == 1)
	{
		return mPower += 3, mVitality += 3;
	}else if(mTitleCounter == 2)
	{
		return mAgility += 3, mDexterity += 3;
	}
	else if (mTitleCounter == 3)
	{
		return mPower += 3;
	}
	
	
}

int Characters::enemyStatsBoost(int boost)
{
	int increase = boost;
	
	cout << "ERandom " << increase << endl;

	return enemyPower += increase, enemyHp += increase, enemyVitality += increase, enemyAgility += increase, enemyDexterity += increase;

	
}

int Characters::healPlayer(int maxHealth)
{
	//Recover health
	int recover;
	recover = ((maxHealth * 30)/100);

	/*cout << "Hp: " << recover << endl;*/
		mHp += recover; 
//Max HP prevent
		if (mHp > maxHealth)
		{
			mHp = maxHealth;
		}
		return mHp;
}

string Characters::getEnemyTitle()
{
	return enemyTitle;
}

int Characters::getEnemyHp()
{
	return enemyHp;
}

int Characters::getEnemyPower()
{
	return enemyPower;
}

int Characters::getEnemyVitality()
{
	return enemyVitality;
}

int Characters::getEnemyAgility()
{
	return enemyAgility;
}

int Characters::getEnemyDexterity()
{
	return enemyDexterity;
}

string Characters::getEnemyTitleCounter(int randomChoice)
{
	/*int randomChoice = rand() % 3 + 1;*/
	if (randomChoice == 1) 
	{
		
		return enemyTitle = "Enemy Warrior";
	}
	else if (randomChoice == 2)
	{
	
		return enemyTitle = "Enemy Assassin";
	}
	else if (randomChoice == 3)
	{
		
		return enemyTitle = "Enemy Mage";
	}

}

int Characters::getEnemyTitleNumber(int randomChoice)
{
	enemyTitleCounter = randomChoice;
	if (randomChoice == 1)
	{
		return enemyTitleCounter == 1;
	} else if (randomChoice == 2)
	{
		return enemyTitleCounter == 2;
	}
	else if(randomChoice == 3)
	{
		return enemyTitleCounter == 3;
	}
}
