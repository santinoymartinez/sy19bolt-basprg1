#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int amount)
{
    mHealthGained = amount;
}

int HealthPotion::getHealthGained()
{
    return mHealthGained;
}

void HealthPotion::useItem(Unit* target)
{
    getHealthPotion(mHealthGained);
}

