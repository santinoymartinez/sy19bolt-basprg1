#pragma once
#include "Item.h"
#include <iostream>
#include <string>
using namespace std;
class HealthPotion :
    public Item
{
public:
    HealthPotion(string name, int amount);
    int getHealthGained();


    virtual void useItem(Unit* target) override;


private:
    int mHealthGained;
};

