#pragma once
#include <time.h>
//#include "Item.h"
#include "Unit.h"
#include "Item.h"
//#include "HealthPotion.h"
//#include "HealthPotion.h"
//#include "Source.h"
using namespace std;
class Unit
{
public:
	Unit(int health, int crystals, int raritypoints);
	int getHp();
	int getCrystals();
	int getRarity();

	Item* stuff();

	Item* getAmount();

private:
	int mHp = 100;
	int mCrystals;
	int mRarity;

	Item* amount;
};

