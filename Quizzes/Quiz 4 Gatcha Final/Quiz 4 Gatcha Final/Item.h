#pragma once
#include "Unit.h"


class Item
{
	Item(int number); 
public:
	int getAmount();
	virtual void useItem(Unit* target) = 0;
	int getHealthPotion(int amount);

private:
	int mAmount;
	int gainedHpAmount;
	


};

