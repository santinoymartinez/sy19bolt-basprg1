#include <iostream>
#include<time.h>
#include<string>
#include <vector>
using namespace std;

int playRound(int number, int &moneyMade, int& Left);
vector<string> cardsEmperor;
vector<string> cardsSlave;
int whichEnding(int money, int length);

int main()
{
	srand(time(0));
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	while (round < 13)
	{
		playRound(round, moneyEarned, mmLeft);
		round++;
	}
	
	cout << moneyEarned << endl;
	whichEnding(moneyEarned, mmLeft);
	
	cout << endl;
	return 0;
}

int playRound(int number, int& moneyMade, int& Left)
{

	int bet;

	int answer;
	string vs;
	system("cls");

	//Current Results

	if (number < 4 || number >= 7 && number <= 9)
	{

		cout << "Cash: " << moneyMade << endl;
		cout << "Distance Left (mm): " << Left << endl;
		cout << "Round: " << number << "/12" << endl;
		cout << "Side: Emperor" << endl;


	}
	else
	{
		cout << "Cash: " << moneyMade << endl;
		cout << "Distance Left (mm): " << Left << endl;
		cout << "Round: " << number << "/12" << endl;
		cout << "Side: Slave" << endl;


	}
	//Betting
	for (int l = Left; Left > 0;)
	{
		while (true)
		{
			cout << "How much mm are you willing to bet Kanji?";
			cin >> bet;
			if (bet > 0 && bet <= Left)
			{
				break;
			}
		}
		system("cls");
		//Vector for both players
		cout << "Pick a card ...." << endl;
		cout << "================" << endl;

		cardsEmperor.push_back("Emperor");
		cardsEmperor.push_back("Emperor");
		cardsEmperor.push_back("Citizen");
		cardsEmperor.push_back("Citizen");
		cardsEmperor.push_back("Citizen");
		cardsEmperor.push_back("Citizen");

		cardsSlave.push_back("Slave");
		cardsSlave.push_back("Slave");
		cardsSlave.push_back("Citizen");
		cardsSlave.push_back("Citizen");
		cardsSlave.push_back("Citizen");
		cardsSlave.push_back("Citizen");

		int counter = 5;
		int& access = counter;
		//Battle
		if (number < 4 || number >= 7 && number <= 9)
		{
			
			int j = 5;
			//Player Emperor Hand + Results
			while (true)
			{
				
				for (int i = counter; i != 0; i--)
				{
					cout << i << ") " << cardsEmperor[i] << endl;
				}
				if (Left <= 0)
				{
					system("cls");
				}
				cin >> answer;

				system("cls");
				int randomNumber = rand() % counter + 1;
				vs = cardsSlave[randomNumber];

				cout << "Open!" << endl;
				cout << "" << endl;
				cout << "[Kaiji] "<< cardsEmperor[answer] << " vs " << "[Tonegawa] " << vs << endl;

				if (cardsEmperor[answer] == cardsEmperor[1] && vs == cardsSlave[1])
				{
					cout << "You lost! The pin will now drill by " << bet << "mm"<<endl;
					Left -= bet;
					system("pause");
					break;
				}
				else if (cardsEmperor[answer] == cardsEmperor[1] && vs != cardsSlave[1])
				{
					Left += bet;
					if (Left >= 31)
					{
						Left -= bet;
						Left;
					}
					bet *= 100000;
					cout << "You won " << bet <<endl;
					moneyMade += bet;
					
					system("pause");
					break;

				}
				else if (cardsEmperor[answer] != cardsEmperor[1] && vs == cardsSlave[1])
				{
					Left += bet;
					if (Left >= 31)
					{
						Left -= bet;
						Left;
					}
					bet *= 100000;
					cout << "You won " << bet << endl;
					moneyMade += bet;
					
					system("pause");
					break;
				}
				else if (cardsEmperor[answer] != cardsEmperor[1] && vs != cardsSlave[1])
				{
					cout << "Tie" << endl;
					/*cardsEmperor.erase(cardsEmperor.begin() + answer);
					cardsSlave.erase(cardsSlave.begin() + randomNumber)*/;
					access--;

				}

			}
		}
		else
		{
			//Player Slave Hand + Results
			while (true)
			{

				for (int i = counter; i != 0; i--)
				{
					cout << i << ") " << cardsSlave[i] << endl;

				}
				cin >> answer;

				int randomNumber = rand() % counter + 1;
				vs = cardsEmperor[randomNumber];
				system("cls");
				cout << "Open!" << endl;
				cout << "" << endl;
				cout << "[Kaiji] " << cardsEmperor[answer] << " vs " << "[Tonegawa] " << vs << endl;

				if (cardsSlave[answer] == cardsSlave[1] && vs == cardsEmperor[1])
				{
					Left += bet;
					if (Left >= 31)
					{
						Left -= bet;
						Left;
					}
					bet *= 500000;
					cout << "You won " << bet << endl;
					moneyMade += bet;
					
					/*cout << moneyMade << endl;*/
					system("pause");
					break;
				}
				else if (cardsSlave[answer] == cardsSlave[1] && vs != cardsEmperor[1])
				{
					cout << "You lost! The pin will now drill by " << bet << "mm" << endl;
					Left -= bet;
					system("pause");
					break;
				}
				else if (cardsSlave[answer] != cardsSlave[1] && vs == cardsEmperor[1])
				{
					cout << "You lost! The pin will now drill by " << bet << "mm" << endl;
					Left -= bet;
					system("pause");
					break;
				}
				else if (cardsSlave[answer] != cardsSlave[1] && vs != cardsEmperor[1])
				{
					cout << "Tie" << endl;
					/*	cardsSlave.erase(cardsSlave.begin() + answer);
						cardsEmperor.erase(cardsEmperor.begin() + randomNumber);*/
					access--;
				}

			}
		}
		return 0;

	}

}
// Deciding Ending
int whichEnding(int money, int length)
{
	system("cls");
	if (money >= 20000000 && length > 0)
	{
		cout << "Best Ending" << endl;
		cout << "Congrats you have reached your goal with your ear unharmed!" << endl;
		system("pause");
	}
	else if (money < 20000000 && length > 0)
	{
		cout << "Meh Ending" << endl;
		cout << "You didn't collect the 20 million" << endl;
		system("pause");
	}
	else
	{
		cout << "Bad Ending" << endl;
		cout << "The drill has pierced your ear!" << endl;
		system("pause");
	}
	return 0;
}